package com.example.smarthouse.smarthouse;

public interface AsyncTaskCompleteListener<T> {
    public void onTaskComplete(T result);
}
