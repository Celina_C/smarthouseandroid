package com.example.smarthouse.smarthouse;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.LegendRenderer;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.DataPointInterface;
import com.jjoe64.graphview.series.LineGraphSeries;
import com.jjoe64.graphview.series.OnDataPointTapListener;
import com.jjoe64.graphview.series.Series;

import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;


public class CreateGraphClass {

    Activity activity;
    Context context;
    String nameSensor;

    public CreateGraphClass(Activity activityy, Context contextt, String nameSensorr) {

        activity = activityy;
        context = contextt;
        nameSensor = nameSensorr;

    }

    public void createGraph(GraphView graph, String dateFrom, String dateTo) {


        ArrayList<MeasurementClass> strREST = new ArrayList<>();

        URLString url = new URLString();

        GraphREST rest = new GraphREST();

        try {
            Net net = new Net();
            if (!net.netBoolean(activity)) {

                Toast.makeText(context, "Brak połączenia z siecią",
                        Toast.LENGTH_LONG).show();

            } else {
                strREST.addAll(rest.execute(url.urlGraph + nameSensor + "/" + dateFrom + "/" + dateTo).get());
                if (strREST.size() != 0) {
                    drawGraph(graph, strREST);
                } else {

                    Toast.makeText(context, "Brak danych potrzebnych do narysowania diagramu.",
                            Toast.LENGTH_LONG).show();
                }


                Log.i("size strREST ", String.valueOf(strREST.size()));
            }

        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
    }

    public void drawGraph(GraphView graph, ArrayList<MeasurementClass> strREST) {

        LineGraphSeries<DataPoint> series = new LineGraphSeries<>();
        LineGraphSeries<DataPoint> serieshumidity = new LineGraphSeries<>();

        int j = 0;
        for (int i = strREST.size() - 1; i >= 0; i--) {

            series.appendData(new DataPoint(j, strREST.get(i).temperature), true, 100);
            j++;
        }

        int z = 0;
        for (int i = strREST.size() - 1; i >= 0; i--) {
            serieshumidity.appendData(new DataPoint(z, strREST.get(i).humidity), true, 100);
            z++;

        }

        series.setOnDataPointTapListener(new OnDataPointTapListener() {
            @Override
            public void onTap(Series series, DataPointInterface dataPoint) {
                Toast.makeText(activity, "Punkt: " + dataPoint.getX() + ", " + dataPoint.getY() + " C", Toast.LENGTH_SHORT).show();
            }
        });

        serieshumidity.setOnDataPointTapListener(new OnDataPointTapListener() {
            @Override
            public void onTap(Series series, DataPointInterface dataPoint) {
                Toast.makeText(activity, "Punkt: " + dataPoint.getX() + ", " + dataPoint.getY() + " %RH", Toast.LENGTH_SHORT).show();
            }
        });

        series.setColor(Color.parseColor("#c79daf"));
        serieshumidity.setColor(Color.parseColor("#16242d"));
        series.setTitle("Temperatura");
        serieshumidity.setTitle("Wilgotność");
        graph.addSeries(series);
        graph.addSeries(serieshumidity);

        graph.getLegendRenderer().setVisible(true);
        graph.getLegendRenderer().setAlign(LegendRenderer.LegendAlign.TOP);

        graph.getViewport().setYAxisBoundsManual(true);
        graph.getViewport().setMinY(5);
        graph.getViewport().setMaxY(100);
        graph.getViewport().setScrollable(true);

        strREST.clear();


    }

    public class GraphREST extends AsyncTask<String, String, ArrayList<MeasurementClass>> {
        /*
        String url;
        public GraphREST(String urll){
            url = urll;
        }
*/
        @Override
        protected ArrayList<MeasurementClass> doInBackground(String... params) {

            byte[] result;
            ArrayList<MeasurementClass> str = new ArrayList<>();
            String stringREST;
            HttpClient client = new DefaultHttpClient();

            HttpGet getRequest = new HttpGet(params[0]);
            try {
                HttpResponse response = client.execute(getRequest);
                StatusLine statusLine = response.getStatusLine();
                if (statusLine.getStatusCode() == HttpURLConnection.HTTP_OK) {

                    result = EntityUtils.toByteArray(response.getEntity());
                    stringREST = new String(result, "UTF-8");
                    JSONArray jsonArray = new JSONArray(stringREST);

                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject json = jsonArray.getJSONObject(i);

                        str.add(new MeasurementClass(json.getString("humidity"), json.getString("temperature")));
                    }

                    Log.i("String REST Measurement", stringREST);
                }
            } catch (IOException | JSONException e) {
                e.printStackTrace();
            }
            return str;
        }
    }
}
