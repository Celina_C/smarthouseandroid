package com.example.smarthouse.smarthouse;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.HashMap;


public class FirstActivity extends ActionBarActivity {

    public static final String PREFS_NAME = "MyPrefsFile";
    private static final String PREF_USERNAME = "username";
    private static final String PREF_PASSWORD = "password";
    HashMap<String, String> data;
    EditText username;
    EditText password;
    CheckBox remember;
    Button login;
    String xUsername;
    String xPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first);
        //Action Bar Color

        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#663347")));
        Typeface font = Typeface.createFromAsset(getAssets(), "Sansation-Light.ttf");
        SharedPreferences pref = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        xUsername = pref.getString(PREF_USERNAME, null);
        xPassword = pref.getString(PREF_PASSWORD, null);

        username = (EditText) findViewById(R.id.username);
        password = (EditText) findViewById(R.id.password);

        login = (Button) findViewById(R.id.login);
        remember = (CheckBox) findViewById(R.id.checkBox);

        username.setTypeface(font);
        password.setTypeface(font);
        remember.setTypeface(font);

        if (xUsername != null && xPassword != null) {
            username.setText(xUsername);
            password.setText(xPassword);
            remember.setChecked(true);
        }

        remember.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!remember.isChecked()) {
                    username.setText("");
                    password.setText("");
                    getSharedPreferences(PREFS_NAME, MODE_PRIVATE)
                            .edit()
                            .putString(PREF_USERNAME, null)
                            .putString(PREF_PASSWORD, null)
                            .commit();
                }
            }
        });


        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                data = new HashMap<>();

                data.put("username", username.getText().toString());
                data.put("password", password.getText().toString());
                if (remember.isChecked()) {
                    getSharedPreferences(PREFS_NAME, MODE_PRIVATE)
                            .edit()
                            .putString(PREF_USERNAME, data.get("username"))
                            .putString(PREF_PASSWORD, data.get("password"))
                            .commit();
                }

                Net net = new Net();
                if (!net.netBoolean(FirstActivity.this)) {

                    Toast.makeText(getApplicationContext(), "Brak połączenia z siecią",
                            Toast.LENGTH_LONG).show();

                } else {
                    URLString url = new URLString();
                    AsyncTaskListener as = new AsyncTaskListener();
                    as.launchTask(url.urlLogin);
                }
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        return super.onOptionsItemSelected(item);
    }

    public class AsyncTaskListener implements AsyncTaskCompleteListener<String> {
        @Override
        public void onTaskComplete(String result) {
            Log.i("Resultat", result);
            if (result.equals("Logged")) {
                Intent intent = new Intent(FirstActivity.this, MeasurementActivity.class);
                startActivity(intent);

            } else {
                Toast.makeText(getApplicationContext(), result,
                        Toast.LENGTH_LONG).show();
                username.getText().clear();
                password.getText().clear();
            }
        }

        public void launchTask(String url) {
            LoginREST loginR = new LoginREST(data, FirstActivity.this, this);
            loginR.execute(url);
        }
    }

    public class LoginREST extends AsyncTask<String, String, String> {
        public ProgressDialog dialog;
        Activity activity;
        private AsyncTaskCompleteListener<String> callback;
        private HashMap<String, String> mData = null;// post data

        public LoginREST(HashMap<String, String> data, Activity activity1, AsyncTaskCompleteListener<String> cb) {
            mData = data;
            activity = activity1;

            callback = cb;
        }

        @Override
        public void onPreExecute() {
            super.onPreExecute();
            dialog = ProgressDialog.show(FirstActivity.this, "Loading",
                    "Please wait...", true);
        }

        @Override
        public void onPostExecute(String result) {
            super.onPostExecute(result);
            if (dialog.isShowing())
                dialog.dismiss();
            callback.onTaskComplete(result);

        }

        @Override
        public String doInBackground(String... params) {
            byte[] result;
            String str = "";
            String stringREST;
            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(params[0]);// in this case, params[0] is URL
            try {
                // set up post data
                ArrayList<NameValuePair> nameValuePair = new ArrayList<>();
                for (String key : mData.keySet()) {
                    nameValuePair.add(new BasicNameValuePair(key, mData.get(key)));
                }

                post.setEntity(new UrlEncodedFormEntity(nameValuePair, "UTF-8"));
                HttpResponse response = client.execute(post);
                StatusLine statusLine = response.getStatusLine();
                if (statusLine.getStatusCode() == HttpURLConnection.HTTP_OK) {
                    result = EntityUtils.toByteArray(response.getEntity());
                    stringREST = new String(result, "UTF-8");
                    JSONObject json = new JSONObject(stringREST);
                    str = json.getString("information");

                    //Log.i("String REST", str);
                }

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (Exception ignored) {
            }
            return str;
        }

    }
}
