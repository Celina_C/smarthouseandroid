package com.example.smarthouse.smarthouse;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;

import com.jjoe64.graphview.GraphView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


public class GraphActivity extends ActionBarActivity implements View.OnClickListener {


    Button graphB;

    String nameSensor;
    EditText dateEditText;
    EditText dateEditTextTo;
    private DatePickerDialog fromDatePicker;
    private DatePickerDialog toDatePicker;

    private SimpleDateFormat dateFormatter;

    public static String getCalculatedDate(SimpleDateFormat dateFormat, int days) {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DAY_OF_YEAR, days);
        return dateFormat.format(new Date(cal.getTimeInMillis()));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_graph);

        //Action Bar Color
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#663347")));

        Intent inte = getIntent();
        Bundle b = inte.getExtras();

        if (b != null) {
            nameSensor = (String) b.get("nameSensor");

        }


        dateEditText = (EditText) findViewById(R.id.edit_date);
        dateEditText.setInputType(InputType.TYPE_NULL);
        dateEditText.requestFocus();

        dateEditTextTo = (EditText) findViewById(R.id.edit_date_to);
        dateEditTextTo.setInputType(InputType.TYPE_NULL);


        setDateTimeField();

        final GraphView graph = (GraphView) findViewById(R.id.graph);


        dateFormatter = new SimpleDateFormat("yyyy-MM-dd");

        String fromDateString = getCalculatedDate(dateFormatter, 0);

        String toDateString = getCalculatedDate(dateFormatter, 1);

        final CreateGraphClass graphClass = new CreateGraphClass(GraphActivity.this, getApplicationContext(), nameSensor);
        graphClass.createGraph(graph, fromDateString, toDateString);

        Log.i("GRaph", fromDateString);
        graphB = (Button) findViewById(R.id.buttonGraph);

        graphB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String dateFromm = String.valueOf(dateEditText.getText());
                String dateToo = String.valueOf(dateEditTextTo.getText());
                graph.removeAllSeries();
                graphClass.createGraph(graph, dateFromm, dateToo);

            }
        });


    }

    private void setDateTimeField() {
        dateEditText.setOnClickListener(this);
        dateEditTextTo.setOnClickListener(this);

        Calendar newCalendar = Calendar.getInstance();
        fromDatePicker = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                dateEditText.setText(dateFormatter.format(newDate.getTime()));
            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

        toDatePicker = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                dateEditTextTo.setText(dateFormatter.format(newDate.getTime()));
            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
    }

    @Override
    public void onClick(View v) {
        if (v == dateEditText) {
            fromDatePicker.show();
        } else if (v == dateEditTextTo) {
            toDatePicker.show();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_graph, menu);
        return true;
    }

    public void refreshButton() {
        Intent intent = getIntent();
        overridePendingTransition(0, 0);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        finish();
        overridePendingTransition(0, 0);
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_refresh) {
            refreshButton();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
