package com.example.smarthouse.smarthouse;


import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;


public class MeasurementActivity extends ActionBarActivity {


    ListView listView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_measurement);

        //Action Bar Color

        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#663347")));

        listView = (ListView) findViewById(R.id.listViewXml);

        Net net = new Net();

        if (!net.netBoolean(MeasurementActivity.this)) {

            Toast.makeText(getApplicationContext(), "Brak połączenia z siecią.",
                    Toast.LENGTH_LONG).show();


        } else {

            URLString url = new URLString();

            MeasurementREST rest = new MeasurementREST();

            try {

                final ArrayList<MeasurementClass> strREST = rest.execute(url.urlMeasurement).get();

                listView.setAdapter(new MeasurementsAdapter(this, strREST));
                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Intent intent = new Intent(MeasurementActivity.this, GraphActivity.class);
                        intent.putExtra("nameSensor", strREST.get(position).nameSensor);
                        startActivity(intent);
                    }
                });
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_measurement, menu);
        return true;
    }

    public void refreshButton() {
        Intent intent = getIntent();
        overridePendingTransition(0, 0);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        finish();
        overridePendingTransition(0, 0);
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_refresh) {
            refreshButton();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public class MeasurementREST extends AsyncTask<String, String, ArrayList<MeasurementClass>> {

        @Override
        protected ArrayList<MeasurementClass> doInBackground(String... params) {
            byte[] result;
            ArrayList<MeasurementClass> str = new ArrayList<>();
            String stringREST;
            HttpClient client = new DefaultHttpClient();

            HttpGet getRequest = new HttpGet(params[0]);


            try {
                HttpResponse response = client.execute(getRequest);
                StatusLine statusLine = response.getStatusLine();
                if (statusLine.getStatusCode() == HttpURLConnection.HTTP_OK) {

                    result = EntityUtils.toByteArray(response.getEntity());
                    stringREST = new String(result, "UTF-8");
                    JSONArray jsonArray = new JSONArray(stringREST);

                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject json = jsonArray.getJSONObject(i);
                        JSONObject jsonSensor = new JSONObject(json.getString("idSensor"));
                        str.add(new MeasurementClass(json.getString("humidity"), json.getString("temperature"),
                                json.getString("timestamp"), jsonSensor.getString("nameSensor"), jsonSensor.getString("idRoom")));
                    }


                }
            } catch (IOException | JSONException e) {
                e.printStackTrace();
            }

            return str;
        }

    }
}
