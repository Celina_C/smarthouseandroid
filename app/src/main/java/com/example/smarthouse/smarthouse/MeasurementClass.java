package com.example.smarthouse.smarthouse;

public class MeasurementClass {
    public double humidity;
    public double temperature;
    public String timestamp;
    public String nameSensor;
    public String nameRoom;

    public MeasurementClass(String xx, String yy) {
        humidity = Double.valueOf(xx);
        temperature = Double.valueOf(yy);
    }


    public MeasurementClass(String x, String y, String z, String name, String r) {
        humidity = Double.valueOf(x);
        temperature = Double.valueOf(y);
        timestamp = z;
        nameSensor = name;
        nameRoom = r;
    }
}
